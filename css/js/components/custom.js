$(function() {

    var itm = $("#headerbasket #basket_count").text().replace('item', '');
    itm = $("#headerbasket #basket_count").text().replace('items', '');
    $('#navbar-basket').html(itm);

    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header


    var tagScroller = $('.js-add-tag-scroller');
    if (tagScroller.length == 1) {
        $('form label[for=save_custom_design_sort_button]').parent('form').css({ "display": "none" });
    }




    function _moveBanner() {
        if ($('.block-banner').length) {
            $('.block-banner').insertBefore($('#mid'));
        }
    }

    function _moveBlock2() {
        if ($('.block2').length) {
            $('.block2').insertBefore($('#mid'));
        }
    }

    function _bodyDesign() {
        if ($("#body_design")) {
            var feedbackcompany = $('#card_order_info .box-footer .feedbackcompany');
            $('#card_order_info .box-footer').append(feedbackcompany);
        }
    }

    function _sidebar() {
        var $mid = $('#mid');
        var $sidebar = $mid.find('#sidebar');
        var $pagecontent = $mid.find('#pagecontent');
        var $dataSidebar = $pagecontent.find('[data-sidebar]');


        if ($dataSidebar.length) {
            $dataSidebar.data('sidebar').split("|").map(function(response) {
                if ($sidebar.find('#' + response).length) {
                    var $menuItem = $sidebar.find('#' + response);
                    $menuItem.show();
                }
            });
        }

        if ($('#sidebar')) {
            if ($("nav #taggroupul-kleuren")) {
                $("nav #taggroupul-kleuren li.kleuren").addClass("kleur");
            }

            $("nav #filter_cards").prepend('<p class="sidebar-title">Filters</p><a class="filter-w" href="javascript:;"><i class="fa fa-refresh"></i>Filters wissen</a>');
        }
    }

    function _accordionFilter() {
        var $accordion = $('.tag-group-ul>li');

        $accordion.each(function() {
            var _self = $(this);
            var openAccordion = _self.find('h3');
            var accordionBody = _self.find('ul');

            $('<i class="fa"></i>').appendTo(openAccordion);

            if (accordionBody.hasClass('show')) {
                _self.find('h3').addClass('active');
                openAccordion.find('.fa').addClass('fa-angle-down');
            } else {
                _self.find('h3').removeClass('active');
                openAccordion.find('.fa').addClass('fa-angle-right');
            }

        });

        $accordion.find('h3').on('click', function(e) {
            e.preventDefault();
            var _self = $(this);
            var target = _self.closest($accordion).find('ul');
            var fa = _self.find('.fa');

            target.toggleClass('show');

            if (target.is(":visible")) {
                _self.addClass('active');
                fa.removeClass('fa-angle-right').addClass('fa-angle-down');
            } else {
                _self.removeClass('active');
                fa.removeClass('fa-angle-down').addClass('fa-angle-right');
            }

        });
    }

    function _fixKleur() {
        if ($('.kleur')) {
            $('.kleur .tag-li').each(function() {
                var $hoverText = $(this).find('label').clone().children().remove().end().text().trim();
                $(this).find('label').attr("title", $hoverText);
            });
        }
    }

    function _hideListAcc() {
        var $btnBlock = $("body[id^='body_account'] ul.list-vertical li a");
        // $btnBlock.each(function(){
        //     switch ($(this).attr('href')) {
        //         case "/account_adresboek":
        //         case "/account_adres_import":
        //             $(this).parent('li').remove();
        //             break;
        //     }
        // });

        var $accountPage = $("body[id='body_account'] #pagebody .button-block a");
        // $accountPage.each(function(){
        //     switch ($(this).attr('href')) {
        //         case '/account_address':
        //         case '/account_adres_import':
        //             $(this).parent().parent().remove();
        //             break;
        //     }
        // });
    }

    _moveBanner();
    _moveBlock2();
    _bodyDesign();
    _sidebar();
    _accordionFilter();
    _fixKleur();
    _hideListAcc();

});

/* Global variables and functions */
var Salut = (function($, window, undefined) {
    'use strict';
    var $win = $(window);
    var $mid = $('#mid');
    var $sidebar = $mid.find('#sidebar');
    var $pagecontent = $mid.find('#pagecontent');

    function _scrollTo($target) {
        $('html, body').animate({
            scrollTop: $target.offset().top
        }, 1000);
    }

    $('#card_previews_horizontal').parent().addClass('cardrail');

    function _detectUrl(e) {
        if (location.hash) {
            setTimeout(function() {

                window.scrollTo(0, 0);
            }, 1);

            _scrollTo($(location.hash));
        }
    }

    function _bindEvent() {
        var timeOutResize;

        $win.on('load', function() {

        });

        var currentWidth = $win.width();
        $win.on('resize', function() {
            clearTimeout(timeOutResize);
            timeOutResize = setTimeout(function() {
                var newWidth = $win.width();

                if (newWidth !== currentWidth) {
                    // Add function after resize windows
                    currentWidth = newWidth;
                }
            }, 200);
        });
    }



    return {
        init: function() {
            _detectUrl();
            _bindEvent();
        },
    };
}(jQuery, window));

jQuery(document).ready(function() {
    Salut.init();
});

jQuery(window).load(function() {
    if ($('.waterfall_grid').length) {
        $('.waterfall_grid').waterfall({
            colMinWidth: 250,
            autoresize: true
        });
    }
});