$(function () {

    var $accordion = $('.accordion');

    $accordion.each(function () {
        var _self = $(this);
        var openAccordion = _self.find('.accordion-button');
        var accordionBody = _self.find('.accordion-body');

        $('<i class="fa"></i>').appendTo(openAccordion);

        if (accordionBody.hasClass('show')) {
            _self.find('accordion-button').addClass('active');
            openAccordion.find('.fa').addClass('fa-angle-down');
        } else {
            _self.find('accordion-button').removeClass('active');
            openAccordion.find('.fa').addClass('fa-angle-right');
        }

    });

    $accordion.find('.accordion-button').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($accordion).find('.accordion-body');
        var fa = _self.find('.fa');

        target.toggleClass('show');
        
        if(target.is(":visible")){
            _self.addClass('active');
            fa.removeClass('fa-angle-right').addClass('fa-angle-down');
        } else {
            _self.removeClass('active');
            fa.removeClass('fa-angle-down').addClass('fa-angle-right');
        }
        
    });

});
